#include <iostream>



class stack
{


private:

	int last_index = 0;
	int size = 0;
	int* array = nullptr;

public:
	stack()
	{}
	

	stack(int new_size)
	{
		array = new int[new_size];
		size = new_size;
		last_index = 0;
	}
	

	void pop()
	{
		if (last_index)
		{
			last_index--;
		}
	}
	void push(int a)
	{
		array[last_index] = a;
		last_index++;

	}
	void show()
	{
		std::cout << array[last_index - 1] << "\n";
	}

	~stack()
	{
		delete[] array;
	}

};



int main()
{

	stack a(3);
	a.push(1); 
	a.show();
	a.push(2);
	a.show();
	a.push(3);
	a.show();
	a.pop();
	a.show();

}



